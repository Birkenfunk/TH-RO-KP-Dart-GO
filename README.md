# TH-RO-KP-Dart-GO
[![status-badge](https://woodpecker.birkenfunk.de/api/badges/2/status.svg)](https://woodpecker.birkenfunk.de/repos/2)
# About

This is a project for the PSTA of the lecture "Konzepte der Programmiersprachen" at the Th-Rosenheim.

The project for the PSTA is a small chat with a server and a client. The backend is developed in go and the Frontend in Flutter/ Dart.

# How to start

## Backend

For the Backend you can ether use docker-compose or the native App

### Docker-Compose

You can ether use a Postgres, MySql or Sqlite Database.

Please replace the `JWT_SECRET` with your own secret

#### Posgres

```yaml
version: '3.8'
services:
  db:
    image: postgres:16.0-alpine
    restart: always
    environment:
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
    volumes:
      - db:/var/lib/postgresql/data
  backend:
    image: codeberg.org/birkenfunk/th-ro-kp-dart-go/backend:latest
    restart: always
    ports:
      - '3000:3000'
    environment:
      - DB_HOST=db
      - DB_PORT=5432
      - DB_USER=postgres
      - DB_PASSWORD=postgres
      - DB_NAME=postgres
      - DB_TYPE=postgres
      - JWT_SECRET=secret
volumes:
  db:
    driver: local

```
 
 #### SqLite

```yaml
version: '3.8'
services:
  backend:
    image: codeberg.org/birkenfunk/th-ro-kp-dart-go/backend:latest
    restart: always
    ports:
      - '3000:3000'
    environment:
      - DB_TYPE=sqlite
      - JWT_SECRET=secret

```

### Native 

Please download the file for your system from
https://codeberg.org/Birkenfunk/TH-RO-KP-Dart-GO/releases/

You can configure the Server via environment variables. Ether you set them your self or you use a `.env` file.

Example for a .env file.

Please replace the `JWT_SECRET` with your own secret
```.env
DB_USER=postgres
DB_PASSWORD=postgres
DB_HOST=localhost
DB_PORT=5432
DB_NAME=postgres
# Possible Types are postgres, mysql, sqlite
DB_TYPE=sqlite
JWT_SECRET=secret
```

If you are using a sqlite database you only need to set the environment variables`DB_TYPE` and `JWT_SECRET`

## Frontend

For the Frontend you have to build it your self at the moment

| :exclamation:  Only tested with linux   |
|-----------------------------------------|

```shell
cd ./Frontend
# Linux
flutter build linux --release
# file is under Frontend/build/linux/x64/release/bundle/frontend

# Windows (can only be executed under Windows host)
flutter build windows

# MacOs (can only be executed in MacOs)
flutter build macos
```

| :exclamation:  Due to a Bug in the http package it's not possible to use the Web App   |
|-----------------------------------------|