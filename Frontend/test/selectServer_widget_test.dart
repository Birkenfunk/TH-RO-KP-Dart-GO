import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/server/SelectServerPage.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/ServerUrl.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import 'widget_test.mocks.dart';

void main() {
  testSelectServerState();
}

void testSelectServerState() {
  return testWidgets('Widget Test for Select Server State', (WidgetTester tester) async {

    final mockObserver = MockNavigatorObserver();
    final mockApiProvider = MockApiProvider();
    final mockApi = MockApiI();

    // when the api is called with the url localhost:3000, it should return true else false
    when(mockApi.isValidServer(any)).thenAnswer((_) => Future.value(false));
    when(mockApiProvider.api).thenReturn(mockApi);

    ApiProvider apiProvider = mockApiProvider;

    final standardRoute = MaterialPageRoute(builder: (_) => const SelectServerPage());
    final routeTo = MaterialPageRoute(builder: (_) => Container());

    Widget testWidget = MultiProvider(providers: [
      ChangeNotifierProvider(create: (context) => apiProvider),
      ChangeNotifierProvider(create: (context) => ServerUrlProvider()),
    ],
        child: MaterialApp(
          navigatorObservers: [mockObserver],
          initialRoute: '/',
          onGenerateRoute: (settings) {
            if (settings.name == '/') {
              return standardRoute;
            } else {
              return routeTo;
            }
          },
        )
    );


    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget);

    // Verify that our counter starts at 0.
    expect(find.text('Select Server'), findsOneWidget);
    expect(find.text('Please enter some text'), findsNothing);

    // Tap the Next button.
    await tester.tap(find.byType(ElevatedButton));
    await tester.pump();

    expect(find.text('Select Server'), findsOneWidget);
    expect(find.text('Please enter a valid url'), findsOneWidget);

    // Input localhost:8080 into the text field.
    await tester.enterText(find.byType(TextFormField), 'randomServer:3000');
    await tester.pump();

    expect(find.text('randomServer:3000'), findsOneWidget);

    // Tap the Next button.
    await tester.tap(find.byType(ElevatedButton));
    await tester.pump();


    expect(find.text('Select Server'), findsOneWidget);
    expect(find.text('Server not reachable'), findsOneWidget);
    expect(find.text('Please enter some text'), findsNothing);
    //verify that the navigator has not changed
    verify(mockObserver.didPush(standardRoute, any)).called(1);

    // Input localhost:8080 into the text field.
    await tester.enterText(find.byType(TextFormField), 'localhost:3000');
    await tester.pump();

    expect(find.text('localhost:3000'), findsOneWidget);

    when(mockApi.isValidServer(any)).thenAnswer((_) => Future.value(true));

    // Tap the Next button.
    await tester.tap(find.byType(ElevatedButton));
    await tester.pump();


    expect(find.text('Select Server'), findsOneWidget);
    expect(find.text('Server not reachable'), findsNothing);
    expect(find.text('Please enter some text'), findsNothing);


    verify(mockObserver.didPush(routeTo, any));
  });
}
