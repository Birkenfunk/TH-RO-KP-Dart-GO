import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/services/errors/Conflict.dart';

void main(){
  test('Test To String', () {
    expect(Conflict('asd').toString(), 'Conflict: asd');
  });
}