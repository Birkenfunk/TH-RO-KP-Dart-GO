import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/services/errors/Conflict.dart';
import 'package:frontend/services/errors/Unauthorized.dart';

void main(){
  test('Test To String', () {
    expect(Unauthorized('asd').toString(), 'Unauthorized: asd');
  });
}