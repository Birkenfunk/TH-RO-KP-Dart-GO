import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/services/Api.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';

void main(){
  test('Loggedin is false from the beginning', () {
    final loginProvider = LoggedInProvider();
    expect(loginProvider.loggedIn, false);
  });
  test('Change Loggedin Works', () {
    final loginProvider = LoggedInProvider();
    loginProvider.changeLoggedIn(true);
    expect(loginProvider.loggedIn, true);
  });
}