import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/services/Api.dart';
import 'package:frontend/services/provider/ApiProvider.dart';

void main(){
  test('Api is filled from the beginning', () {
    final apiProvider = ApiProvider();
    expect(apiProvider.api, isNotNull);
    expect(apiProvider.api is Api, true);
  });
}