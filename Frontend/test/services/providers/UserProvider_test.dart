import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/dtos/User.dart';
import 'package:frontend/services/Api.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/UserProvider.dart';

void main(){
  test('Api is null from the beginning', () {
    final userProvider = UserProvider();
    expect(userProvider.user, null);
  });
  test('User is saved', () {
    final userProvider = UserProvider();
    User user = User('name', 'password');
    userProvider.changeUser(user);
    expect(userProvider.user, user);
  });
  test('user can be set to null', (){
    final userProvider = UserProvider();
    User user = User('name', 'password');
    userProvider.changeUser(user);
    expect(userProvider.user, user);
    userProvider.changeUser(null);
    expect(userProvider.user, null);
  });
}