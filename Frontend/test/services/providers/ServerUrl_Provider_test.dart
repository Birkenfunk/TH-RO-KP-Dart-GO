import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/dtos/User.dart';
import 'package:frontend/services/Api.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/ServerUrl.dart';
import 'package:frontend/services/provider/UserProvider.dart';

void main(){
  test('Server urs is null from the beginning', () {
    final serverUrlProvider = ServerUrlProvider();
    expect(serverUrlProvider.url, null);
  });
  test('Url is saved', () {
    final serverUrlProvider = ServerUrlProvider();
    String url = 'http://localhost:8080';
    serverUrlProvider.changeUrl(url);
    expect(serverUrlProvider.url, url);
  });
  test('user can be set to null', (){
    final serverUrlProvider = ServerUrlProvider();
    String url = 'http://localhost:8080';
    serverUrlProvider.changeUrl(url);
    expect(serverUrlProvider.url, url);
    serverUrlProvider.changeUrl(null);
    expect(serverUrlProvider.url, null);
  });
}