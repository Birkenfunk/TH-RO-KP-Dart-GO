import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/services/provider/ChatProvider.dart';
import 'package:http/http.dart';

void main(){
  test('AllFields are null from the beginning', () {
    final chatProvider = ChatProvider();
    expect(chatProvider.chatRoom, null);
    expect(chatProvider.stream, null);
  });
  test('Chatroom is saved', () {
    final chatProvider = ChatProvider();
    String chatRoom = 'chatRoom';
    chatProvider.changeChatroom(chatRoom);
    expect(chatProvider.chatRoom, chatRoom);
    expect(chatProvider.stream, null);
  });
  test('Chatroom can be set to null', (){
    final chatProvider = ChatProvider();
    String chatRoom = 'chatRoom';
    chatProvider.changeChatroom(chatRoom);
    expect(chatProvider.chatRoom, chatRoom);
    expect(chatProvider.stream, null);
    chatProvider.changeChatroom(null);
    expect(chatProvider.chatRoom, null);
    expect(chatProvider.stream, null);
  });
  test('Stream is saved', () {
    final chatProvider = ChatProvider();
    Stream<String> stream = const Stream.empty();
    chatProvider.changeStream(stream);
    expect(chatProvider.chatRoom, null);
    expect(chatProvider.stream, stream);
  });
  test('Stream can be set to null', (){
    final chatProvider = ChatProvider();
    Stream<String> stream = const Stream.empty();
    chatProvider.changeStream(stream);
    expect(chatProvider.chatRoom, null);
    expect(chatProvider.stream, stream);
    chatProvider.changeStream(null);
    expect(chatProvider.chatRoom, null);
    expect(chatProvider.stream, null);
  });
}