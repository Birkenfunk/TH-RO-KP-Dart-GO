import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/dtos/User.dart';
import 'package:frontend/dtos/token.dart';
import 'package:frontend/services/Api.dart';
import 'package:frontend/services/errors/Conflict.dart';
import 'package:frontend/services/errors/Unauthorized.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'Api_test.mocks.dart';


@GenerateNiceMocks([MockSpec<Dio>()])
void main() {
  isValidServerFalseTest();
  isValidServerTrueTest();
  isValidServerExceptionTest();
  createUserTestSuccess();
  createUserTestConflict();
  loginTestSuccess();
  loginTestUnauthorized();
  changePasswordTestSuccess();
  changePasswordTestUnauthorized();
  registerForChatTestSuccess();
  registerForChatTestUnauthorized();
  registerForChatTestConflict();
  postMessageTestSuccess();
  postMessageTestUnauthorized();
}

void postMessageTestUnauthorized() {
  return test('Post Message throws Unauthorized', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.post(any, data: anyNamed('data'), options: anyNamed('options'))).thenThrow(DioException(
        requestOptions: RequestOptions(path: ''),
        response: Response(statusCode: 401, requestOptions: RequestOptions(path: ''))));
    expect(() async => await api.postMessage('test', 'message'),
        throwsA(isInstanceOf<Unauthorized>()));
  });
}

void postMessageTestSuccess() {
  return test('Post Message returns token', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.post('/api/v1/chat/test', data: anyNamed('data'))).thenAnswer((_) async =>
        Response(data: {},
            statusCode: 200,
            requestOptions: RequestOptions(path: '')));
    //expect no exception
    expect(() async => await api.postMessage('test', 'message'),
        returnsNormally);
  });
}

void registerForChatTestConflict() {
  return test('Register for Chat throws Conflict', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.get(any, options: anyNamed('options'))).thenThrow(DioException(
        requestOptions: RequestOptions(path: ''),
        response: Response(statusCode: 409, requestOptions: RequestOptions(path: ''))));
    expect(() async => await api.registerForChatRoom('test'),
        throwsA(isInstanceOf<Conflict>()));
  });
}

void registerForChatTestUnauthorized() {
  return test('Register for Chat throws Unauthorized', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.get(any, options: anyNamed('options'))).thenThrow(DioException(
        requestOptions: RequestOptions(path: ''),
        response: Response(statusCode: 401, requestOptions: RequestOptions(path: ''))));
    expect(() async => await api.registerForChatRoom('test'),
        throwsA(isInstanceOf<Unauthorized>()));
  });
}

void registerForChatTestSuccess() {
  return test('Register for Chat returns stream', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.get<ResponseBody>(any, options: anyNamed('options'))).thenAnswer((_) async =>
        Response(data: ResponseBody(Stream.fromIterable([Uint8List.fromList([50, 50, 50])]), 200),
            statusCode: 200,
            requestOptions: RequestOptions(path: '')));
    Stream<String> result = await api.registerForChatRoom('test');
    expect(result, isNotNull);
  });
}

void changePasswordTestUnauthorized() {
  return test('Change Password throws Unauthorized', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.put(any, data: anyNamed('data'), options: anyNamed('options'))).thenThrow(DioException(
        requestOptions: RequestOptions(path: ''),
        response: Response(statusCode: 401, requestOptions: RequestOptions(path: ''))));
    expect(() async => await api.changePassword(UpdateUserDto('name', 'password', 'newPassword')),
        throwsA(isInstanceOf<Unauthorized>()));
  });
}

void changePasswordTestSuccess() {
  return test('Change Password returns token', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.post('/api/v1/users/name', data: anyNamed('data'))).thenAnswer((_) async =>
        Response(data: {},
            statusCode: 200,
            requestOptions: RequestOptions(path: '')));
    //expect no exception
    expect(() async => await api.changePassword(UpdateUserDto('name', 'password', 'newPassword')),
        returnsNormally);
  });
}

void loginTestUnauthorized() {
  return test('Login throws Unauthorized', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.post(any, data: anyNamed('data'))).thenThrow(DioException(
        requestOptions: RequestOptions(path: ''),
        response: Response(statusCode: 401, requestOptions: RequestOptions(path: ''))));
    expect(() async => await api.login(User('name', 'password')),
        throwsA(isInstanceOf<Unauthorized>()));
  });
}

void loginTestSuccess() {
  return test('Login returns token', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.post('/api/v1/login', data: anyNamed('data'))).thenAnswer((_) async =>
        Response(data: {'token': 'token'},
            statusCode: 200,
            requestOptions: RequestOptions(path: '')));
    Token result = await api.login(User('name', 'password'));
    expect(result, Token(token: 'token'));
  });
}

void createUserTestConflict() {
  return test('Create User throws Conflict', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.post(any, data: anyNamed('data'))).thenThrow(DioError(
        requestOptions: RequestOptions(path: ''),
        response: Response(statusCode: 409, requestOptions: RequestOptions(path: ''))));
    expect(() async => await api.createUser(User('name', 'password')),
        throwsA(isInstanceOf<Conflict>()));
  });
}

void createUserTestSuccess() {
  return test('Create User returns token', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.post('/api/v1/users', data: anyNamed('data'))).thenAnswer((_) async =>
        Response(data: {},
            statusCode: 200,
            requestOptions: RequestOptions(path: '')));
    when(dio.post('/api/v1/login', data: anyNamed('data'))).thenAnswer((_) async =>
        Response(data: {'token': 'token'},
            statusCode: 200,
            requestOptions: RequestOptions(path: '')));
    Token result = await api.createUser(User('name', 'password'));
    expect(result, Token(token: 'token'));
  });
}

void isValidServerExceptionTest() {
  return test('Is Valid Server throws exception', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.get(any)).thenThrow(Exception());
    expect(await api.isValidServer('http://localhost:8080'), false);
  });
}

void isValidServerTrueTest() {
  return test('Is Valid Server returns true', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.get(any)).thenAnswer((_) async =>
        Response(data: null,
            statusCode: 200,
            requestOptions: RequestOptions(path: '')));
    expect(await api.isValidServer('http://localhost:8080'), true);
  });
}

void isValidServerFalseTest() {
  return test('Is Valid Server returns flase', () async {
    MockDio dio = MockDio();
    final api = Api.test(dio);
    when(dio.get(any)).thenAnswer((_) async =>
        Response(data: null,
            statusCode: 404,
            requestOptions: RequestOptions(path: '')));
    expect(await api.isValidServer('http://localhost:8080'), false);
  });
}