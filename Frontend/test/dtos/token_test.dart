import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/dtos/token.dart';

void main(){
  test('Token To Json', () => testTokenToJson());
  test('Token From Json', () => testTokenFromJson());
}

void testTokenFromJson() {
  String json = '{"token":"randomToken"}';

  Token token = Token.fromJson(jsonDecode(json));

  expect(token.token, 'randomToken');
}

void testTokenToJson() {
  Token token = Token(token: "randomToken");
  String result = json.encode(token.toJson());
  expect(result, '{"token":"randomToken"}');
}