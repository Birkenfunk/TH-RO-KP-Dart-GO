import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/dtos/User.dart';

void main(){
  test('User To Json', () => testUserToJson());
  test('User From Json', () => testUserFromJson());
  test('Update User To Json', () => testUpdateUserToJson());
  test('Update User From Json', () => testUpdateUserFromJson());
  test ('UserDto To Json', () => testUserDtoToJson());
  test ('UserDto From Json', () => testUserDtoFromJson());
}

void testUserFromJson() {
  String json = '{"username":"name","password":"password"}';

  User result = User.fromJson(jsonDecode(json));

  expect(result.username, 'name');
  expect(result.password, 'password');
}

void testUserToJson() {
  User user = User('name', 'password');
  String result = json.encode(user.toJson());
  expect(result, '{"username":"name","password":"password"}');
}

testUpdateUserToJson() {
  UpdateUserDto updateUserDto = UpdateUserDto('name', 'currentPassword', 'newPassword');
  String result = json.encode(updateUserDto.toJson());
  expect(result, '{"username":"name","current_password":"currentPassword","password":"newPassword"}');
}

testUpdateUserFromJson() {
  String json = '{"username":"name","current_password":"currentPassword","password":"newPassword"}';

  UpdateUserDto result = UpdateUserDto.fromJson(jsonDecode(json));

  expect(result.username, 'name');
  expect(result.currentPassword, 'currentPassword');
  expect(result.newPassword, 'newPassword');
}

testUserDtoToJson(){
  UserDto userDto = UserDto(1, 'name', DateTime.utc(2021, 5, 1, 12), DateTime.utc(2021, 5, 1, 12));
  String result = json.encode(userDto.toJson());
  expect(result, '{"id":1,"username":"name","createdAt":"2021-05-01 12:00:00.000Z","updatedAt":"2021-05-01 12:00:00.000Z"}');
}

testUserDtoFromJson(){
  String json = '{"id":1,"username":"name","createdAt":"2021-05-01 12:00:00.000Z","updatedAt":"2021-05-01 12:00:00.000Z"}';

  UserDto result = UserDto.fromJson(jsonDecode(json));

  expect(result.id, 1);
  expect(result.username, 'name');
  expect(result.createdAt, DateTime.parse('2021-05-01T12:00:00.000Z'));
  expect(result.updatedAt, DateTime.parse('2021-05-01T12:00:00.000Z'));
}