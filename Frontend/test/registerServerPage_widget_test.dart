import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/dtos/token.dart';
import 'package:frontend/register/RegisterPage.dart';
import 'package:frontend/services/errors/Conflict.dart';
import 'package:frontend/services/errors/Unauthorized.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';
import 'package:frontend/services/provider/ServerUrl.dart';
import 'package:frontend/services/provider/UserProvider.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import 'widget_test.mocks.dart';

void main() {
  testRegisterPageNoInput();
  testRegisterPageWithWrongData();
  testRegisterPageCorrectData();
  testRegisterPageSwitchToLogin();
  testRegisterPageWithUsernameTaken();
}

void testRegisterPageNoInput() {
  return testWidgets('Widget Test for LoginState State with no Input to press Submit', (WidgetTester tester) async {
    final mockObserver = MockNavigatorObserver();
    final mockApiProvider = MockApiProvider();
    final mockApi = MockApiI();

    // when the api is called with the url localhost:3000, it should return true else false
    when(mockApi.isValidServer(any)).thenAnswer((_) => Future.value(false));
    when(mockApiProvider.api).thenReturn(mockApi);

    ApiProvider apiProvider = mockApiProvider;

    final standardRoute = MaterialPageRoute(
        builder: (_) => const RegisterPage());
    final routeTo = MaterialPageRoute(builder: (_) => Container());

    Widget testWidget = MultiProvider(providers: [
      ChangeNotifierProvider(create: (context) => apiProvider),
      ChangeNotifierProvider(create: (context) => ServerUrlProvider()),
    ],
        child: MaterialApp(
          navigatorObservers: [mockObserver],
          initialRoute: '/register',
          onGenerateRoute: (settings) {
            if (settings.name == '/register') {
              return standardRoute;
            } else {
              return routeTo;
            }
          },
        )
    );

    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget);

    verify(mockObserver.didPush(standardRoute, any));

    // Verify that Page is LoginPage
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);

    // tab submit button
    await tester.tap(find.byKey(const Key('register_submit_button')));
    await tester.pump();
    
    // Verify that Page is LoginPage
    expect(find.text('Login'), findsOneWidget);
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('Please enter some text'), findsNWidgets(3));

  });
}

void testRegisterPageWithWrongData() {
  return testWidgets('Widget Test for LoginState State with wrong Input', (WidgetTester tester) async {
    final mockObserver = MockNavigatorObserver();
    final mockApiProvider = MockApiProvider();
    final mockApi = MockApiI();
    final mockLoginProvider = MockLoggedInProvider();
    final mockUserProvider = MockUserProvider();

    // when the api is called with the url localhost:3000, it should return true else false
    when(mockApi.isValidServer(any)).thenAnswer((_) => Future.value(false));
    when(mockApiProvider.api).thenReturn(mockApi);

    final standardRoute = MaterialPageRoute(
        builder: (_) => const RegisterPage());
    final routeTo = MaterialPageRoute(builder: (_) => Container());

    Widget testWidget = MultiProvider(providers: [
      ChangeNotifierProvider(create: (context) => mockApiProvider as ApiProvider),
      ChangeNotifierProvider(create: (context) => ServerUrlProvider()),
      ChangeNotifierProvider(create: (context) => mockLoginProvider as LoggedInProvider),
      ChangeNotifierProvider(create: (context) => mockUserProvider as UserProvider),
    ],
        child: MaterialApp(
          navigatorObservers: [mockObserver],
          initialRoute: '/register',
          onGenerateRoute: (settings) {
            if (settings.name == '/register') {
              return standardRoute;
            } else {
              return routeTo;
            }
          },
        )
    );

    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget);

    verify(mockObserver.didPush(standardRoute, any));

    // Verify that Page is LoginPage
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);

    // Input username and password
    await tester.enterText(find.byKey(const Key('register_username_input')), 'test');
    await tester.enterText(find.byKey(const Key('register_password_input')), 'test');
    await tester.enterText(find.byKey(const Key('register_password_again_input')), 'test2');
    await tester.pump();

    // Verify that Page is LoginPage
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('test'), findsExactly(2));

    // tab submit button
    when(mockApi.login(any)).thenThrow(Unauthorized(''));
    await tester.tap(find.byKey(const Key('register_submit_button')));
    await tester.pump();

    // Verify that Page is LoginPage
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('test'), findsExactly(2));
    expect(find.text('test2'), findsOneWidget);
    expect(find.text('Passwords do not match'), findsOneWidget);


  });
}

void testRegisterPageWithUsernameTaken() {
  return testWidgets('Widget Test for LoginState State with wrong Input', (WidgetTester tester) async {
    final mockObserver = MockNavigatorObserver();
    final mockApiProvider = MockApiProvider();
    final mockApi = MockApiI();
    final mockLoginProvider = MockLoggedInProvider();
    final mockUserProvider = MockUserProvider();

    // when the api is called with the url localhost:3000, it should return true else false
    when(mockApi.isValidServer(any)).thenAnswer((_) => Future.value(false));
    when(mockApiProvider.api).thenReturn(mockApi);

    final standardRoute = MaterialPageRoute(
        builder: (_) => const RegisterPage());
    final routeTo = MaterialPageRoute(builder: (_) => Container());

    Widget testWidget = MultiProvider(providers: [
      ChangeNotifierProvider(create: (context) => mockApiProvider as ApiProvider),
      ChangeNotifierProvider(create: (context) => ServerUrlProvider()),
      ChangeNotifierProvider(create: (context) => mockLoginProvider as LoggedInProvider),
      ChangeNotifierProvider(create: (context) => mockUserProvider as UserProvider),
    ],
        child: MaterialApp(
          navigatorObservers: [mockObserver],
          initialRoute: '/register',
          onGenerateRoute: (settings) {
            if (settings.name == '/register') {
              return standardRoute;
            } else {
              return routeTo;
            }
          },
        )
    );

    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget);

    verify(mockObserver.didPush(standardRoute, any));

    // Verify that Page is LoginPage
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);

    // Input username and password
    await tester.enterText(find.byKey(const Key('register_username_input')), 'test');
    await tester.enterText(find.byKey(const Key('register_password_input')), 'test');
    await tester.enterText(find.byKey(const Key('register_password_again_input')), 'test');
    await tester.pump();

    // Verify that Page is LoginPage
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('test'), findsExactly(3));

    // tab submit button
    when(mockApi.createUser(any)).thenThrow(Conflict(''));
    await tester.tap(find.byKey(const Key('register_submit_button')));
    await tester.pump();

    // Verify that Page is LoginPage
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('test'), findsExactly(3));
    expect(find.text('Username already taken'), findsOneWidget);


  });
}

void testRegisterPageCorrectData() {
  return testWidgets('Widget Test for LoginState State with Correct Input', (WidgetTester tester) async {
    final mockObserver = MockNavigatorObserver();
    final mockApiProvider = MockApiProvider();
    final mockApi = MockApiI();
    final mockLoginProvider = MockLoggedInProvider();
    final mockUserProvider = MockUserProvider();

    // when the api is called with the url localhost:3000, it should return true else false
    when(mockApi.isValidServer(any)).thenAnswer((_) => Future.value(false));
    when(mockApiProvider.api).thenReturn(mockApi);

    final standardRoute = MaterialPageRoute(
        builder: (_) => const RegisterPage());
    final routeTo = MaterialPageRoute(builder: (_) => Container());

    Widget testWidget = MultiProvider(providers: [
      ChangeNotifierProvider(create: (context) => mockApiProvider as ApiProvider),
      ChangeNotifierProvider(create: (context) => ServerUrlProvider()),
      ChangeNotifierProvider(create: (context) => mockLoginProvider as LoggedInProvider),
      ChangeNotifierProvider(create: (context) => mockUserProvider as UserProvider),
    ],
        child: MaterialApp(
          navigatorObservers: [mockObserver],
          initialRoute: '/register',
          onGenerateRoute: (settings) {
            if (settings.name == '/register') {
              return standardRoute;
            } else {
              return routeTo;
            }
          },
        )
    );

    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget);

    verify(mockObserver.didPush(standardRoute, any));

    // Verify that Page is LoginPage
    expect(find.text('Login'), findsOneWidget);
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);

    // Input username and password
    await tester.enterText(find.byKey(const Key('register_username_input')), 'test');
    await tester.enterText(find.byKey(const Key('register_password_input')), 'test');
    await tester.enterText(find.byKey(const Key('register_password_again_input')), 'test');
    await tester.pump();

    // Verify that Page is LoginPage
    expect(find.text('Login'), findsOneWidget);
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('test'), findsExactly(3));

    // tab submit button
    when(mockApi.createUser(any)).thenAnswer((_) => Future.value(Token(token: 'someToken')));
    await tester.tap(find.byKey(const Key('register_submit_button')));
    await tester.pump();

    // Verify that Page is LoginPage
    verify(mockObserver.didPop(standardRoute, any));
    verify(mockObserver.didPush(routeTo, any));
    verify(mockApi.createUser(any)).called(1);
    verify(mockLoginProvider.changeLoggedIn(true)).called(1);
    verify(mockUserProvider.changeUser(any)).called(1);
  });
}

void testRegisterPageSwitchToLogin() {
  return testWidgets('Widget Test for LoginState State with Correct Input', (WidgetTester tester) async {
    final mockObserver = MockNavigatorObserver();
    final mockApiProvider = MockApiProvider();
    final mockApi = MockApiI();
    final mockLoginProvider = MockLoggedInProvider();
    final mockUserProvider = MockUserProvider();

    // when the api is called with the url localhost:3000, it should return true else false
    when(mockApi.isValidServer(any)).thenAnswer((_) => Future.value(false));
    when(mockApiProvider.api).thenReturn(mockApi);

    final standardRoute = MaterialPageRoute(
        builder: (_) => const RegisterPage());
    final routeTo = MaterialPageRoute(builder: (_) => Container());
    final routeToLogin = MaterialPageRoute(builder: (_) => Container());

    Widget testWidget = MultiProvider(providers: [
      ChangeNotifierProvider(create: (context) => mockApiProvider as ApiProvider),
      ChangeNotifierProvider(create: (context) => ServerUrlProvider()),
      ChangeNotifierProvider(create: (context) => mockLoginProvider as LoggedInProvider),
      ChangeNotifierProvider(create: (context) => mockUserProvider as UserProvider),
    ],
        child: MaterialApp(
          navigatorObservers: [mockObserver],
          initialRoute: '/register',
          onGenerateRoute: (settings) {
            if (settings.name == '/register') {
              return standardRoute;
            } else if (settings.name == '/login') {
              return routeToLogin;
            } else {
              return routeTo;
            }
          },
        )
    );

    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget);

    verify(mockObserver.didPush(standardRoute, any));

    // Verify that Page is LoginPage
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Login'), findsOneWidget);

    // tab login button
    await tester.tap(find.byKey(const Key('register_login_button')));
    await tester.pump();

    // Verify that Page is LoginPage
    verify(mockObserver.didPop(standardRoute, any));
    verify(mockObserver.didPush(routeToLogin, any));
  });
}