// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.
import 'package:flutter/material.dart';
import 'package:frontend/services/ApiI.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';
import 'package:frontend/services/provider/UserProvider.dart';
import 'package:mockito/annotations.dart';

import 'loginServerPage_widget_test.dart';
import 'selectServer_widget_test.dart';


@GenerateNiceMocks([MockSpec<NavigatorObserver>(), MockSpec<ApiProvider>(), MockSpec<ApiI>(), MockSpec<LoggedInProvider>(), MockSpec<UserProvider>()])
void main() {
}

