import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:frontend/dtos/token.dart';
import 'package:frontend/login/LoginPage.dart';
import 'package:frontend/services/errors/Unauthorized.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';
import 'package:frontend/services/provider/ServerUrl.dart';
import 'package:frontend/services/provider/UserProvider.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import 'widget_test.mocks.dart';

void main() {
  testLoginServerStateNoInput();
  testLoginServerStateWithWrongData();
  testLoginServerStateWithCorrectData();
  testLoginServerStateSwitchToRegister();
}

void testLoginServerStateNoInput() {
  return testWidgets('Widget Test for LoginState State with no Input to press Submit', (WidgetTester tester) async {
    final mockObserver = MockNavigatorObserver();
    final mockApiProvider = MockApiProvider();
    final mockApi = MockApiI();

    // when the api is called with the url localhost:3000, it should return true else false
    when(mockApi.isValidServer(any)).thenAnswer((_) => Future.value(false));
    when(mockApiProvider.api).thenReturn(mockApi);

    ApiProvider apiProvider = mockApiProvider;

    final standardRoute = MaterialPageRoute(
        builder: (_) => const LoginPage());
    final routeTo = MaterialPageRoute(builder: (_) => Container());

    Widget testWidget = MultiProvider(providers: [
      ChangeNotifierProvider(create: (context) => apiProvider),
      ChangeNotifierProvider(create: (context) => ServerUrlProvider()),
    ],
        child: MaterialApp(
          navigatorObservers: [mockObserver],
          initialRoute: '/login',
          onGenerateRoute: (settings) {
            if (settings.name == '/login') {
              return standardRoute;
            } else {
              return routeTo;
            }
          },
        )
    );

    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget);

    verify(mockObserver.didPush(standardRoute, any));

    // Verify that Page is LoginPage
    expect(find.text('Login'), findsExactly(2));
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);

    // tab submit button
    await tester.tap(find.byKey(const Key('login_submit_button')));
    await tester.pump();
    
    // Verify that Page is LoginPage
    expect(find.text('Login'), findsExactly(2));
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('Please enter some text'), findsNWidgets(2));

  });
}

void testLoginServerStateWithWrongData() {
  return testWidgets('Widget Test for LoginState State with wrong Input', (WidgetTester tester) async {
    final mockObserver = MockNavigatorObserver();
    final mockApiProvider = MockApiProvider();
    final mockApi = MockApiI();
    final mockLoginProvider = MockLoggedInProvider();
    final mockUserProvider = MockUserProvider();

    // when the api is called with the url localhost:3000, it should return true else false
    when(mockApi.isValidServer(any)).thenAnswer((_) => Future.value(false));
    when(mockApiProvider.api).thenReturn(mockApi);

    final standardRoute = MaterialPageRoute(
        builder: (_) => const LoginPage());
    final routeTo = MaterialPageRoute(builder: (_) => Container());

    Widget testWidget = MultiProvider(providers: [
      ChangeNotifierProvider(create: (context) => mockApiProvider as ApiProvider),
      ChangeNotifierProvider(create: (context) => ServerUrlProvider()),
      ChangeNotifierProvider(create: (context) => mockLoginProvider as LoggedInProvider),
      ChangeNotifierProvider(create: (context) => mockUserProvider as UserProvider),
    ],
        child: MaterialApp(
          navigatorObservers: [mockObserver],
          initialRoute: '/login',
          onGenerateRoute: (settings) {
            if (settings.name == '/login') {
              return standardRoute;
            } else {
              return routeTo;
            }
          },
        )
    );

    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget);

    verify(mockObserver.didPush(standardRoute, any));

    // Verify that Page is LoginPage
    expect(find.text('Login'), findsExactly(2));
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);

    // Input username and password
    await tester.enterText(find.byKey(const Key('login_username_input')), 'test');
    await tester.enterText(find.byKey(const Key('login_password_input')), 'test');
    await tester.pump();

    // Verify that Page is LoginPage
    expect(find.text('Login'), findsExactly(2));
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('test'), findsExactly(2));

    // tab submit button
    when(mockApi.login(any)).thenThrow(Unauthorized(''));
    await tester.tap(find.byKey(const Key('login_submit_button')));
    await tester.pump();

    // Verify that Page is LoginPage
    expect(find.text('Login'), findsExactly(2));
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('test'), findsExactly(2));
    expect(find.text('Wrong username or password'), findsOneWidget);


  });
}

void testLoginServerStateWithCorrectData() {
  return testWidgets('Widget Test for LoginState State with Correct Input', (WidgetTester tester) async {
    final mockObserver = MockNavigatorObserver();
    final mockApiProvider = MockApiProvider();
    final mockApi = MockApiI();
    final mockLoginProvider = MockLoggedInProvider();
    final mockUserProvider = MockUserProvider();

    // when the api is called with the url localhost:3000, it should return true else false
    when(mockApi.isValidServer(any)).thenAnswer((_) => Future.value(false));
    when(mockApiProvider.api).thenReturn(mockApi);

    final standardRoute = MaterialPageRoute(
        builder: (_) => const LoginPage());
    final routeTo = MaterialPageRoute(builder: (_) => Container());

    Widget testWidget = MultiProvider(providers: [
      ChangeNotifierProvider(create: (context) => mockApiProvider as ApiProvider),
      ChangeNotifierProvider(create: (context) => ServerUrlProvider()),
      ChangeNotifierProvider(create: (context) => mockLoginProvider as LoggedInProvider),
      ChangeNotifierProvider(create: (context) => mockUserProvider as UserProvider),
    ],
        child: MaterialApp(
          navigatorObservers: [mockObserver],
          initialRoute: '/login',
          onGenerateRoute: (settings) {
            if (settings.name == '/login') {
              return standardRoute;
            } else {
              return routeTo;
            }
          },
        )
    );

    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget);

    verify(mockObserver.didPush(standardRoute, any));

    // Verify that Page is LoginPage
    expect(find.text('Login'), findsExactly(2));
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);

    // Input username and password
    await tester.enterText(find.byKey(const Key('login_username_input')), 'test');
    await tester.enterText(find.byKey(const Key('login_password_input')), 'test');
    await tester.pump();

    // Verify that Page is LoginPage
    expect(find.text('Login'), findsExactly(2));
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);
    expect(find.text('test'), findsExactly(2));

    // tab submit button
    when(mockApi.login(any)).thenAnswer((_) => Future.value(Token(token: 'someToken')));
    await tester.tap(find.byKey(const Key('login_submit_button')));
    await tester.pump();

    // Verify that Page is LoginPage
    verify(mockObserver.didPop(standardRoute, any));
    verify(mockObserver.didPush(routeTo, any));
    verify(mockLoginProvider.changeLoggedIn(true)).called(1);
    verify(mockUserProvider.changeUser(any)).called(1);
  });
}

void testLoginServerStateSwitchToRegister() {
  return testWidgets('Widget Test for LoginState State with Correct Input', (WidgetTester tester) async {
    final mockObserver = MockNavigatorObserver();
    final mockApiProvider = MockApiProvider();
    final mockApi = MockApiI();
    final mockLoginProvider = MockLoggedInProvider();
    final mockUserProvider = MockUserProvider();

    // when the api is called with the url localhost:3000, it should return true else false
    when(mockApi.isValidServer(any)).thenAnswer((_) => Future.value(false));
    when(mockApiProvider.api).thenReturn(mockApi);

    final standardRoute = MaterialPageRoute(
        builder: (_) => const LoginPage());
    final routeTo = MaterialPageRoute(builder: (_) => Container());
    final routeToRegister = MaterialPageRoute(builder: (_) => Container());

    Widget testWidget = MultiProvider(providers: [
      ChangeNotifierProvider(create: (context) => mockApiProvider as ApiProvider),
      ChangeNotifierProvider(create: (context) => ServerUrlProvider()),
      ChangeNotifierProvider(create: (context) => mockLoginProvider as LoggedInProvider),
      ChangeNotifierProvider(create: (context) => mockUserProvider as UserProvider),
    ],
        child: MaterialApp(
          navigatorObservers: [mockObserver],
          initialRoute: '/login',
          onGenerateRoute: (settings) {
            if (settings.name == '/login') {
              return standardRoute;
            } else if (settings.name == '/register') {
              return routeToRegister;
            } else {
              return routeTo;
            }
          },
        )
    );

    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget);

    verify(mockObserver.didPush(standardRoute, any));

    // Verify that Page is LoginPage
    expect(find.text('Login'), findsExactly(2));
    expect(find.text('Enter your username'), findsOneWidget);
    expect(find.text('Enter your password'), findsOneWidget);
    expect(find.text('Submit'), findsOneWidget);
    expect(find.text('Register'), findsOneWidget);

    // tab submit button
    when(mockApi.login(any)).thenAnswer((_) => Future.value(Token(token: 'someToken')));
    await tester.tap(find.byKey(const Key('login_register_button')));
    await tester.pump();

    // Verify that Page is LoginPage
    verify(mockObserver.didPop(standardRoute, any));
    verify(mockObserver.didPush(routeToRegister, any));
  });
}