import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:frontend/services/ApiI.dart';
import 'package:frontend/services/errors/Conflict.dart';
import 'package:frontend/services/errors/Unauthorized.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/ChatProvider.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';
import 'package:frontend/services/provider/ServerUrl.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

class CreateChatForm extends StatefulWidget{
  const CreateChatForm({super.key});


  @override
  State<StatefulWidget> createState() => _CreateChatFormState();
}

class _CreateChatFormState extends State<CreateChatForm>{

  final _formKey = GlobalKey<FormState>();

  String _chatRoom = '';
  String _error = '';

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              decoration: const InputDecoration(
                hintText: 'Enter the Chatroom',
              ),
              validator: (String? value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
              onChanged: (String value) {
                setState(() {
                  _chatRoom = value;
                });
              },
            ),
            const SizedBox(height: 10),
            Text(
              _error,
              style: const TextStyle(color: Colors.red),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: () {
                // Validate returns true if the form is valid, or false otherwise.
                if (_formKey.currentState!.validate()) {
                  createChatroom(context.read<ApiProvider>().api,
                      context.read<ChatProvider>());
                }
              },
              child: const Text('Submit'),
            ),
          ],
        )
    );
  }

  Future<void> createChatroom(ApiI api, ChatProvider chatProvider) async {
    try {
      Stream<String> stream = await api.registerForChatRoom(
          _chatRoom);
      chatProvider.changeChatroom(_chatRoom);
      chatProvider.changeStream(stream);
    } on Conflict catch (e) {
      setState(() {
        _error = 'You are already registered for a chatroom';
      });
      return;
    } on Unauthorized catch (e) {
      setState(() {
        _error = 'Not logged in';
      });
      return;
    }
    setState(() {
      Navigator.popAndPushNamed(context, '/chat');
    });
  }
  
}