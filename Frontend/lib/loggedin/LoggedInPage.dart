import 'package:flutter/material.dart';
import 'package:frontend/compontents/StartScafold.dart';
import 'package:frontend/compontents/UserOptions.dart';
import 'package:frontend/loggedin/CreateChatForm.dart';
import 'package:frontend/services/provider/ChatProvider.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';
import 'package:provider/provider.dart';

class LoggedInPage extends StatelessWidget {
  const LoggedInPage({super.key});

  @override
  Widget build(BuildContext context) {
    return StartScaffold(
      title: 'Chat App',
      actions: const [
        UserOptions()
      ],
      child: Column(
        children: [
          Text(context.watch<ChatProvider>().chatRoom ?? ''),
          const CreateChatForm()
        ],
      )
    );
  }
}