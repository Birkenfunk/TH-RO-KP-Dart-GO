import 'package:flutter/material.dart';
import 'package:frontend/services/ApiI.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/ServerUrl.dart';
import 'package:provider/provider.dart';


class SelectServerForm extends StatefulWidget {
  const SelectServerForm({super.key});

  @override
  State<SelectServerForm> createState() => _SelectServerFormState();
}

class _SelectServerFormState extends State<SelectServerForm> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _error = '';

  next(ApiI api, ServerUrlProvider urlProvider) async{
    String? serverUrl = urlProvider.url;
    if(!(serverUrl!.startsWith("http://")|| serverUrl.startsWith("https://"))){
      serverUrl = serverUrl.replaceFirst("", "http://");
    }
    if(!await api.isValidServer(serverUrl)){
      setState(() {
        _error = 'Server not reachable';
      });
      return;
    } else {
      setState(() {
        _error = '';
      });
    }
    urlProvider.changeUrl(serverUrl);
    setState(() {
      Navigator.pushNamed(context, '/login');
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      //half of page
        width: MediaQuery.of(context).size.width * 0.5,
        child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  initialValue: context.watch<ServerUrlProvider>().url,
                  decoration: const InputDecoration(
                    hintText: 'Enter your server url',
                  ),
                  validator: (String? value) {
                    //check if url is valid
                    if(!Uri.parse(value!).isAbsolute){
                      return 'Please enter a valid url';
                    }
                    return null;
                  },
                  onChanged: (String value) {
                    context.read<ServerUrlProvider>().changeUrl(value);
                  },
                ),
                const SizedBox(height: 10),
                Text(
                  _error,
                  style: const TextStyle(color: Colors.red),
                ),
                const SizedBox(height: 10),
                ElevatedButton(
                  onPressed: () {
                    // Validate returns true if the form is valid, or false otherwise.
                    if (_formKey.currentState!.validate()) {
                      next(context.read<ApiProvider>().api, context.read<ServerUrlProvider>());
                    }
                  },
                  child: const Text('Next'),
                ),
              ],
            )
        )
    );
  }
}