import 'package:flutter/material.dart';
import 'package:frontend/dtos/User.dart';
import 'package:frontend/services/ApiI.dart';
import 'package:frontend/services/errors/Unauthorized.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';
import 'package:frontend/services/provider/ServerUrl.dart';
import 'package:frontend/services/provider/UserProvider.dart';
import 'package:provider/provider.dart';

import '../dtos/token.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({super.key});

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _username = '';
  String _password = '';
  String _error = '';

  login(ApiI api, String? url, LoggedInProvider loggedInProvider, UserProvider userProvider) async{
    Token token;
    try {
      token = await api.login(User(_username, _password));
    } catch (e) {
      setState(() {
        if (e is Unauthorized) {
          _error = 'Wrong username or password';
        } else {
          print(e);
          _error = 'Error while logging in';
        }
      });
      return;
    }
    userProvider.changeUser(User(_username, _password));
    loggedInProvider.changeLoggedIn(true);
    setState(() {
      Navigator.pop(context, '/');
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      //half of page
        width: MediaQuery.of(context).size.width * 0.5,
        child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  key: const Key('login_username_input'),
                  decoration: const InputDecoration(
                    hintText: 'Enter your username',
                  ),
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  onChanged: (String value) {
                    setState(() {
                      _username = value;
                    });
                  },
                ),
                const SizedBox(height: 10),
                TextFormField(
                  key: const Key('login_password_input'),
                  decoration: const InputDecoration(
                    hintText: 'Enter your password',
                  ),
                  obscureText: true,
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter some text';
                    }
                    return null;
                  },
                  onChanged: (String value) {
                    setState(() {
                      _password = value;
                    });
                  },
                ),
                const SizedBox(height: 10),
                Text(
                  _error,
                  style: const TextStyle(color: Colors.red),
                ),
                const SizedBox(height: 10),
                ElevatedButton(
                  key: const Key('login_submit_button'),
                  onPressed: () {
                    // Validate returns true if the form is valid, or false otherwise.
                    if (_formKey.currentState!.validate()) {
                      login(context.read<ApiProvider>().api,
                          context.read<ServerUrlProvider>().url,
                          context.read<LoggedInProvider>(),
                          context.read<UserProvider>());
                    }
                  },
                  child: const Text('Submit'),
                ),
              ],
            )
        )
    );
  }
}