import 'package:flutter/material.dart';
import 'package:frontend/compontents/StartScafold.dart';
import 'package:frontend/login/LoginForm.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();


}

class _LoginPageState extends State<LoginPage> {

  @override
  Widget build(BuildContext context) {
    return StartScaffold(
        title: 'Login',
        child: Column(
          children: [
            const SizedBox(height: 10),
            const Text(
              'Login',
              style: TextStyle(fontSize: 20),
            ),
            const SizedBox(height: 10),
            const LoginForm(),
            const SizedBox(height: 10),
            //Button to go to register page
            ElevatedButton(
              key: const Key('login_register_button'),
              onPressed: () {
                Navigator.popAndPushNamed(context, '/register');
              },
              child: const Text('Register'),
            ),
          ],
        )
    );
  }
}

