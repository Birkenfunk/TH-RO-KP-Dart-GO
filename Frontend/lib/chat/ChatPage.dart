import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:frontend/chat/ChatForm.dart';
import 'package:frontend/compontents/StartScafold.dart';
import 'package:frontend/compontents/UserOptions.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/ChatProvider.dart';
import 'package:provider/provider.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({super.key});

  @override
  State<StatefulWidget> createState() => _CreateChatState();
}

class _CreateChatState extends State<ChatPage>{

  List<String> _messages = [];

  @override
  Widget build(BuildContext context) {
    return
      StartScaffold(
        title: 'Chat ${context.watch<ChatProvider>().chatRoom ?? ''}',
        actions: const [
          UserOptions()
        ],
        child: Column(
            children: [
              const ChatForm(),
              const SizedBox(height: 10),
              Expanded(
                child:SafeArea(
                  child:  StreamBuilder(
                      stream: context.watch<ChatProvider>().stream,
                      builder: (context, snapshot) {
                        if (snapshot.connectionState == ConnectionState.waiting || !snapshot.hasData) {
                          return const CircularProgressIndicator();
                        } else if (snapshot.connectionState == ConnectionState.done) {
                          return const Text('done');
                        } else if (snapshot.hasError) {
                          return const Text('Error!');
                        } else {
                          String content = snapshot.requireData;
                          content = content.replaceAll("data: ", '');
                          _messages.add(content);
                          return ListView.builder(
                              itemCount: _messages.length,
                              itemBuilder: (context, index) {
                                return ListTile(
                                  title: Text(_messages[index]),
                                );
                              }
                          );
                        }
                      }
                  ),
                ),
              ),
              const SizedBox(height: 10),
              ElevatedButton(
                onPressed: () {
                  // Validate returns true if the form is valid, or false otherwise.
                  context.read<ApiProvider>().api.unregisterForChatRoom();
                  context.read<ChatProvider>().changeChatroom(null);
                  context.read<ChatProvider>().changeStream(null);
                  Navigator.popAndPushNamed(context, '/');
                },
                child: const Text('Leave Chat'),
              ),
            ]
        ),
      );
  }
}