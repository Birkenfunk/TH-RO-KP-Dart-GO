import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:frontend/services/ApiI.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/ChatProvider.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';
import 'package:frontend/services/provider/ServerUrl.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

class ChatForm extends StatefulWidget{
  const ChatForm({super.key});


  @override
  State<StatefulWidget> createState() => _ChatFormState();
}

class _ChatFormState extends State<ChatForm>{

  final _formKey = GlobalKey<FormState>();

  String _message = '';
  String _error = '';

  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            controller: controller,
            decoration: const InputDecoration(
              hintText: 'Enter the Message',
            ),
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
            onChanged: (String value) {
              setState(() {
                _message = value;
              });
            },
          ),
          const SizedBox(height: 10),
          Text(
            _error,
            style: const TextStyle(color: Colors.red),
          ),
          const SizedBox(height: 10),
          ElevatedButton(
            onPressed: () {
              // Validate returns true if the form is valid, or false otherwise.
              if (_formKey.currentState!.validate()) {
                var chatProvider = context.read<ChatProvider>();
                context.read<ApiProvider>().api.postMessage(chatProvider.chatRoom!, _message);
                setState(() {
                  _message = '';
                });
                controller.clear();
              }
            },
            child: const Text('Send Message'),
          ),
        ],
      ),
    );
  }
  
}