import 'package:flutter/material.dart';
import 'package:frontend/loggedin/LoggedInPage.dart';
import 'package:frontend/server/SelectServerPage.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({super.key});

  @override
  Widget build(BuildContext context) {
    if(!context.watch<LoggedInProvider>().loggedIn){
      return const SelectServerPage(key: Key("LogInScreen"));
    }
    return const LoggedInPage();
  }
}