class User{
  String username;
  String password;
  User(this.username, this.password);

  User.fromJson(Map<String, dynamic> json)
      : username = json['username'],
        password = json['password'];

  Map<String, dynamic> toJson() => {
    'username': username,
    'password': password,
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is User &&
          runtimeType == other.runtimeType &&
          username == other.username &&
          password == other.password;

  @override
  int get hashCode => username.hashCode ^ password.hashCode;
}

class UpdateUserDto{
  String username;
  String currentPassword;
  String newPassword;
  UpdateUserDto(this.username, this.currentPassword, this.newPassword);

  UpdateUserDto.fromJson(Map<String, dynamic> json)
      : username = json['username'],
        currentPassword = json['current_password'],
        newPassword = json['password'];

  Map<String, dynamic> toJson() => {
    'username': username,
    'current_password': currentPassword,
    'password': newPassword,
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UpdateUserDto &&
          runtimeType == other.runtimeType &&
          username == other.username &&
          currentPassword == other.currentPassword &&
          newPassword == other.newPassword;

  @override
  int get hashCode =>
      username.hashCode ^ currentPassword.hashCode ^ newPassword.hashCode;
}

class UserDto{
  int id;
  String username;
  DateTime createdAt;
  DateTime updatedAt;
  UserDto(this.id, this.username, this.createdAt, this.updatedAt);

  UserDto.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        username = json['username'],
        createdAt = DateTime.parse(json['createdAt']),
        updatedAt = DateTime.parse(json['updatedAt']);

  Map<String, dynamic> toJson() => {
    'id': id,
    'username': username,
    'createdAt': createdAt.toString(),
    'updatedAt': updatedAt.toString(),
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserDto &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          username == other.username &&
          createdAt == other.createdAt &&
          updatedAt == other.updatedAt;

  @override
  int get hashCode =>
      id.hashCode ^ username.hashCode ^ createdAt.hashCode ^ updatedAt.hashCode;
}