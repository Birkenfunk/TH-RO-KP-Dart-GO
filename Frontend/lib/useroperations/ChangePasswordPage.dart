import 'package:flutter/cupertino.dart';
import 'package:frontend/compontents/StartScafold.dart';
import 'package:frontend/useroperations/ChangePassowordForm.dart';

class ChangePasswordPage extends StatefulWidget{
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage>{

  @override
  Widget build(BuildContext context) {
    return const StartScaffold(
      title: 'Change Password',
      child: ChangePasswordForm(),
    );
  }
}