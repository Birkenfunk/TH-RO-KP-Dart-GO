import 'package:flutter/material.dart';
import 'package:frontend/dtos/User.dart';
import 'package:frontend/services/ApiI.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';
import 'package:frontend/services/provider/ServerUrl.dart';
import 'package:frontend/services/provider/UserProvider.dart';
import 'package:provider/provider.dart';

class ChangePasswordForm extends StatefulWidget{
  const ChangePasswordForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChangePasswordFormState();
}

class _ChangePasswordFormState extends State<ChangePasswordForm>{

  final _formKey = GlobalKey<FormState>();

  String _oldPassword = "";
  String _newPassword = "";
  String _error = "";

  changePassword(ApiI api, UserProvider userProvider, String url, LoggedInProvider jwtProvider) async{
    try {
      await api.changePassword(UpdateUserDto(userProvider.user!.username, _oldPassword, _newPassword));
    } catch (e) {
      _error = 'Error while changing password';
      print(e);
      return;
    }
    setState(() {
      Navigator.pop(context, '/');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              obscureText: true,
              decoration: const InputDecoration(
                hintText: 'Enter your old password',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter your old password';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  _oldPassword = value;
                });
              },
            ),
            const SizedBox(height: 10),
            TextFormField(
              obscureText: true,
              decoration: const InputDecoration(
                hintText: 'Enter your new password',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter your new password';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  _newPassword = value;
                });
              },
            ),
            const SizedBox(height: 10),
            TextFormField(
              obscureText: true,
              decoration: const InputDecoration(
                hintText: 'Confirm your new password',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please confirm your new password';
                }
                if(value != _newPassword){
                  return 'Passwords do not match';
                }
                return null;
              },
            ),
            const SizedBox(height: 10),
            Text(
              _error,
              style: const TextStyle(color: Colors.red),
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  changePassword(
                      context.read<ApiProvider>().api,
                      context.read<UserProvider>(), 
                      context.read<ServerUrlProvider>().url!, 
                      context.read<LoggedInProvider>()
                  );
                }
              },
              child: const Text('Submit'),
            ),
          ],
        )
    );
  }

}