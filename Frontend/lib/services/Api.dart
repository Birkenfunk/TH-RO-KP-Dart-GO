import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:frontend/dtos/User.dart';
import 'package:frontend/dtos/token.dart';
import 'package:frontend/services/ApiI.dart';
import 'package:frontend/services/errors/Conflict.dart';
import 'package:frontend/services/errors/Unauthorized.dart';
import 'package:http/http.dart' as http;

class Api implements ApiI{
  
  late final Dio _dio;

  Api(){
    _dio = Dio(BaseOptions());
  }

  Api.test(this._dio);

  String _token = '';
  String _serverUrl = '';

  @override
  Future<bool> isValidServer(String url) async {
    try {
      Response response = await _dio.get('$url/api/v1/health');
      if (response.statusCode == 200) {
        _serverUrl = url;
        return true;
      }
    } catch (e){
      if (kDebugMode) {
        print(url);
      }
      return false;
    }
    return false;
  }

  @override
  Future<Token> createUser(User user) async{
    try{
      await _dio.post('$_serverUrl/api/v1/users', data: json.encode(user));
    } on DioException catch(e){
      if(e.response?.statusCode == 409){
        throw Conflict('');
      }
    }
    return login(user);
  }

  @override
  Future<Token> login(User user) async {
    Response?  response;
    try {
      response = await _dio.post('$_serverUrl/api/v1/login', data: json.encode(user.toJson()));
    } on DioException catch(e){
      if (e.response?.statusCode == 401) {
        throw Unauthorized('');
      }
    }
    Token token = Token.fromJson(response?.data);
    _token = token.token;
    return token;
  }

  @override
  Future<void> changePassword(UpdateUserDto updateUserDto) async {
    try {
      //Send put to /api/v1/users with auth Bear token in header and body with old and new password
      Options options = Options(headers: {'Authorization': 'Bearer $_token'});
      await _dio.put('$_serverUrl/api/v1/users', data: json.encode(updateUserDto.toJson()), options: options);
    } on DioException catch(e){
      if (e.response?.statusCode == 401) {
        throw Unauthorized('');
      }
      throw Exception(e);
    }
  }

  @override
  Future<Stream<String>> registerForChatRoom(String chatroom) async {
    Stream<String> byteSteam;
    try {
      Options options = Options(headers: {'Authorization': 'Bearer $_token', 'Accept': 'text/event-stream'}, responseType: ResponseType.stream);
      var response = await _dio.get<ResponseBody>('$_serverUrl/api/v1/chat/register/$chatroom', options: options);

      byteSteam = response.data!.stream
          .transform(unit8Transformer)
          .transform(const Utf8Decoder());
    } on DioException catch(e){
      if (kDebugMode) {
        print(e);

      }if (e.response?.statusCode == 401) {
        throw Unauthorized('');
      }
      if (e.response?.statusCode == 409) {
        throw Conflict('');
      }
      throw Exception(e);
    }
    return byteSteam;
  }

  @override
  Future<void> postMessage(String chatroom, String message) async {
    try {
      Options options = Options(headers: {'Authorization': 'Bearer $_token'});
      var response = await _dio.post('$_serverUrl/api/v1/chat/send/$chatroom', data: json.encode({'message': message}), options: options);
    } on DioException catch(e){
      if (e.response?.statusCode == 401) {
        throw Unauthorized('');
      }
    }
  }

  @override
  void unregisterForChatRoom(){
    http.Client().close();
  }

  @override
  bool isLoggedOut() {
    return _token.isEmpty;
  }

  StreamTransformer<Uint8List, List<int>> unit8Transformer =
  StreamTransformer.fromHandlers(
    handleData: (data, sink) {
      sink.add(List<int>.from(data));
    },
  );

}