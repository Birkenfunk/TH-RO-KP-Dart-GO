class UnreachableError extends Error {
  final String message;

  UnreachableError(this.message);

  @override
  String toString() => 'UnreachableError: $message';
}