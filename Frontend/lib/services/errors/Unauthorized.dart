class Unauthorized extends Error {
  final String message;

  Unauthorized(this.message);

  @override
  String toString() => 'Unauthorized: $message';
}