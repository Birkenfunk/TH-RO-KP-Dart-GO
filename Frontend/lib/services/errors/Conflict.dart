class Conflict extends Error {
  final String message;

  Conflict(this.message);

  @override
  String toString() => 'Conflict: $message';
}