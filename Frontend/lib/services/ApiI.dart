import 'package:frontend/dtos/token.dart';
import 'package:http/http.dart';

import '../dtos/User.dart';

abstract interface class ApiI {
  //check if the server is valid
  Future<bool> isValidServer(String url);

  //User operations
  Future<Token> createUser(User user);
  Future<void> changePassword(UpdateUserDto updateUserDto);

  //Auth operations
  Future<Token> login(User user);

  //Chat operations
  Future<Stream<String>> registerForChatRoom(String chatroom);
  void unregisterForChatRoom();
  Future<void> postMessage(String chatroom, String message);

  bool isLoggedOut();
}