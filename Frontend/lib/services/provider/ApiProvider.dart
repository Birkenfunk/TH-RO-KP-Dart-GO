import 'package:flutter/cupertino.dart';
import 'package:frontend/services/Api.dart';
import 'package:frontend/services/ApiI.dart';

class ApiProvider extends ChangeNotifier {
  final ApiI _api = Api();

  ApiI get api => _api;

  ApiProvider();
}