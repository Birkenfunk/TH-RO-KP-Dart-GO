import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';

class ChatProvider extends ChangeNotifier{
  String? _chatRoom;
  Stream<String>? _stream;

  String? get chatRoom => _chatRoom;
  Stream<String>? get stream => _stream;

  ChatProvider();

  void changeChatroom(String? value) {
    _chatRoom = value;
    notifyListeners();
  }

  void changeStream(Stream<String>? value) {
    _stream = value;
    notifyListeners();
  }
}