import 'package:flutter/cupertino.dart';
import 'package:frontend/dtos/User.dart';

class UserProvider extends ChangeNotifier {
  User? _user;

  User? get user => _user;

  UserProvider();

  void changeUser(User? user) async {
    _user = user;
    notifyListeners();
  }
}