import 'package:flutter/cupertino.dart';

class LoggedInProvider extends ChangeNotifier {
  bool _loggedIn = false;

  bool get loggedIn => _loggedIn;

  LoggedInProvider();

  void changeLoggedIn(bool value) {
    _loggedIn = value;
    notifyListeners();
  }

}