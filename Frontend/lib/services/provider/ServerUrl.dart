import 'package:flutter/cupertino.dart';

class ServerUrlProvider extends ChangeNotifier {
  String? _url;

  String? get url => _url;

  ServerUrlProvider();

  void changeUrl(String? jwt) async {
    _url = jwt;
    notifyListeners();
  }
}