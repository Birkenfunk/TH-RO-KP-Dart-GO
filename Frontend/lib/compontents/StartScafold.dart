import 'package:flutter/material.dart';

class StartScaffold extends StatelessWidget{
  final Widget child;
  final String title;
  final List<Widget>? actions;
  const StartScaffold({Key? key, required this.child, required this.title, this.actions}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Text(title),
          actions: actions,
        ),
        body: SafeArea(
            child: Center(
                child: SizedBox(
                    //half of page
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: child
                )
            )
        )
    );
  }
}