import 'package:flutter/material.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';
import 'package:provider/provider.dart';

class UserOptions extends StatelessWidget{
  const UserOptions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
        itemBuilder: (context){
          return [
            //logout button
            PopupMenuItem(
              value: 'logout',
              child: TextButton.icon(
                onPressed: () => {
                  context.read<LoggedInProvider>().changeLoggedIn(false),
                  Navigator.popUntil(context, (route) => route.isFirst),
                  Navigator.popAndPushNamed(context, '/')
                },
                icon: const Icon(Icons.logout),
                label: const Text('Logout'),
              ),
            ),
            PopupMenuItem(
              value: 'change Password',
              child: TextButton.icon(
                onPressed: () => Navigator.pushNamed(context, '/changePassword'),
                icon: const Icon(Icons.password),
                label: const Text('Change Password'),
              ),
            )
          ];
        },
        icon: const Icon(Icons.person)
    );
  }

}