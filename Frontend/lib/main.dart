import 'package:flutter/material.dart';
import 'package:frontend/chat/ChatPage.dart';
import 'package:frontend/register/RegisterPage.dart';
import 'package:frontend/services/provider/ApiProvider.dart';
import 'package:frontend/services/provider/ChatProvider.dart';
import 'package:frontend/services/provider/LoggedInProvider.dart';
import 'package:frontend/services/provider/ServerUrl.dart';
import 'package:frontend/services/provider/UserProvider.dart';
import 'package:frontend/useroperations/ChangePasswordPage.dart';
import 'package:provider/provider.dart';

import 'Wrapper/Wrapper.dart';
import 'login/LoginPage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => UserProvider()),
        ChangeNotifierProvider(create: (context) => LoggedInProvider()),
        ChangeNotifierProvider(create: (context) => ServerUrlProvider()),
        ChangeNotifierProvider(create: (context) => ApiProvider()),
        ChangeNotifierProvider(create: (context) => ChatProvider()),
      ],
      child: const ChatApp(),
    );

  }
}

class ChatApp extends StatelessWidget{
  const ChatApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => const Wrapper(),
        '/login': (context) => const LoginPage(),
        '/register': (context) => const RegisterPage(),
        '/changePassword': (context) => const ChangePasswordPage(),
        '/chat': (context) => const ChatPage(),
      },
      title: 'Chat App',
      theme: ThemeData(
      colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
      useMaterial3: true,
    ),
    );
  }
}