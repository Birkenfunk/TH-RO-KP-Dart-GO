import 'package:flutter/material.dart';
import 'package:frontend/compontents/StartScafold.dart';
import 'package:frontend/register/RegisterForm.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();


}

class _RegisterPageState extends State<RegisterPage> {

  @override
  Widget build(BuildContext context) {
    return StartScaffold(
        title: 'Register',
        child: Column(
          children: [
            const SizedBox(height: 10),
            const RegisterForm(),
            const SizedBox(height: 10),
            //Button to go to login page
            ElevatedButton(
              key: const Key('register_login_button'),
              onPressed: () {
                Navigator.popAndPushNamed(context, '/login');
              },
              child: const Text('Login'),
            ),
          ],
        )
    );
  }
}