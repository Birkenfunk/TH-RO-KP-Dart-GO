package dtos

type ChatMessage struct {
	Message string `json:"message"`
}

type ReceivedMessage struct {
	Message  string `json:"message"`
	Username string `json:"username"`
}
