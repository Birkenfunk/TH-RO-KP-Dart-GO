package dtos

// CreateUser is the DTO for creating a user
// swagger:model
// @Description DTO for creating a user
type CreateUser struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type User struct {
	Id        uint   `json:"id"`
	Username  string `json:"username"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type UpdateUser struct {
	Username        string `json:"username"`
	CurrentPassword string `json:"current_password"`
	Password        string `json:"password"`
}

type Login struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
