package models

import (
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	"testing"
	"th-rosenheim/kp/backend/dtos"
	"time"
)

// Test if User is transformed to DTO correctly
func Test_UserToUserDto(t *testing.T) {
	tests := []struct {
		name *User
		want *dtos.User
	}{
		{
			&User{
				Id:        1,
				Username:  "test",
				Password:  "test",
				CreatedAt: time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC),
				UpdatedAt: time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC),
				DeletedAt: gorm.DeletedAt{},
			},
			&dtos.User{
				Id:        1,
				Username:  "test",
				CreatedAt: "2021-01-01 00:00:00 +0000 UTC",
				UpdatedAt: "2021-01-01 00:00:00 +0000 UTC",
			},
		},
		{
			&User{
				Id:        2,
				Username:  "test2",
				Password:  "test2",
				CreatedAt: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
				UpdatedAt: time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC),
				DeletedAt: gorm.DeletedAt{},
			},
			&dtos.User{
				Id:        2,
				Username:  "test2",
				CreatedAt: "2020-01-01 00:00:00 +0000 UTC",
				UpdatedAt: "2021-01-01 00:00:00 +0000 UTC",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name.Username, func(t *testing.T) {
			assert.Equal(t, tt.want, tt.name.ToDtoUser())
		})
	}

}
