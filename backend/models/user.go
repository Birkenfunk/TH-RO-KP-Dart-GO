package models

import (
	"gorm.io/gorm"
	"th-rosenheim/kp/backend/dtos"
	"time"
)

type User struct {
	Id        uint           `form:"id" gorm:"primaryKey, autoIncrement"`
	Username  string         `form:"username" gorm:"not null;unique"`
	Password  string         `form:"password" gorm:"not null"`
	CreatedAt time.Time      `form:"created_at" gorm:"autoCreateTime"`
	UpdatedAt time.Time      `form:"updated_at" gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt `form:"deleted_at" gorm:"index"`
}

func (u User) ToDtoUser() *dtos.User {
	return &dtos.User{
		Id:        u.Id,
		Username:  u.Username,
		CreatedAt: u.CreatedAt.String(),
		UpdatedAt: u.UpdatedAt.String(),
	}
}
