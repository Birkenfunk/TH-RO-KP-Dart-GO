package routes

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/jwtauth/v5"
	"net/http"
	"th-rosenheim/kp/backend/handler"
	"th-rosenheim/kp/backend/handler/auth"
)

func RegisterHandlerV1() chi.Router {
	r := chi.NewRouter()
	r.Mount("/users", RegisterHandlerV1Users())
	r.Mount("/login", RegisterHandlerV1Login())
	r.Mount("/chat", RegisterHandlerV1Chat())
	r.Get("/health", func(w http.ResponseWriter, r *http.Request) {
		handler.HealthCheck(w, r)
	})
	return r
}

func RegisterHandlerV1Chat() http.Handler {
	r := chi.NewRouter()
	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(auth.GetToken()))
		r.Use(jwtauth.Authenticator)
		r.Get("/register/{room}", func(w http.ResponseWriter, r *http.Request) {
			handler.Register(w, r)
		})
		r.Post("/send/{room}", func(w http.ResponseWriter, r *http.Request) {
			handler.SendMessage(w, r)
		})
	})
	return r
}

func RegisterHandlerV1Users() chi.Router {
	r := chi.NewRouter()

	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(auth.GetToken()))
		r.Use(jwtauth.Authenticator)
		r.Use(middleware.AllowContentType("application/json", "text/event-stream"))
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			handler.GetUsers(w, r)
		})
		r.Get("/{id}", func(w http.ResponseWriter, r *http.Request) {
			handler.GetUserById(w, r)
		})
		r.Put("/", func(w http.ResponseWriter, r *http.Request) {
			handler.UpdatePassword(w, r)
		})
		r.Delete("/{id}", func(w http.ResponseWriter, r *http.Request) {
			handler.DeleteUser(w, r)
		})
	})
	r.Group(func(r chi.Router) {
		r.Post("/", func(w http.ResponseWriter, r *http.Request) {
			handler.CreateUser(w, r)
		})
	})
	return r
}

func RegisterHandlerV1Login() chi.Router {
	r := chi.NewRouter()
	r.Post("/", func(w http.ResponseWriter, r *http.Request) {
		handler.Login(w, r)
	})
	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(auth.GetToken()))
		r.Use(jwtauth.Authenticator)
		r.Get("/renew", func(w http.ResponseWriter, r *http.Request) {
			handler.Renew(w, r)
		})
	})
	return r
}
