package routes

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
	"th-rosenheim/kp/backend/dtos"
	"th-rosenheim/kp/backend/handler"
)

func TestLogin(t *testing.T) {
	r := InitRouter()

	loginUser := dtos.Login{
		Username: "test",
		Password: "test",
	}

	createUserJsonString, _ := json.Marshal(loginUser)

	req, _ := http.NewRequest("POST", "/api/v1/login", bytes.NewReader(createUserJsonString))

	req.Header.Add("Content-Type", "application/json")

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusOK, respone.Code)

	assert.Contains(t, respone.Body.String(), "token")
}

func TestLogin_unauth(t *testing.T) {
	r := InitRouter()

	loginUser := dtos.Login{
		Username: "test",
		Password: "test1",
	}

	createUserJsonString, _ := json.Marshal(loginUser)

	req, _ := http.NewRequest("POST", "/api/v1/login", bytes.NewReader(createUserJsonString))

	req.Header.Add("Content-Type", "application/json")

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusUnauthorized, respone.Code)

	assert.NotContains(t, respone.Body.String(), "token")
}

func TestRenew(t *testing.T) {
	r := InitRouter()

	req, _ := http.NewRequest("GET", "/api/v1/login/renew", nil)

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test", t))

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusOK, respone.Code)

	assert.Contains(t, respone.Body.String(), "token")
}

func TestRenew_unauth(t *testing.T) {
	r := InitRouter()

	req, _ := http.NewRequest("GET", "/api/v1/login/renew", nil)

	req.Header.Add("Authorization", "Bearer asdf")

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusUnauthorized, respone.Code)

	assert.Equal(t, "token is unauthorized\n", respone.Body.String())
}
