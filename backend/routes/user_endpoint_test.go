package routes

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/go-chi/chi/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	"th-rosenheim/kp/backend/dtos"
	"th-rosenheim/kp/backend/handler"
	"th-rosenheim/kp/backend/handler/auth"
	"time"
)

func init() {
	err := godotenv.Load("../test.env")
	if err != nil {
		panic(err)
	}
}

type testRepository struct {
}

var user = dtos.User{Id: 1, Username: "test", CreatedAt: "2021-06-01T00:00:00Z", UpdatedAt: "2021-06-01T00:00:00Z"}

func (r *testRepository) GetUserById(id uint) *dtos.User {
	if id == 1 {
		return &user
	} else {
		return nil
	}
}

func (r *testRepository) GetUsers() *[]dtos.User {
	return &[]dtos.User{user}
}

func (r *testRepository) CreateUser(createUser dtos.CreateUser) (*dtos.User, error) {
	if createUser.Username != "test" {
		return &dtos.User{Id: 2, Username: createUser.Username, CreatedAt: "2021-06-01T00:00:00Z", UpdatedAt: "2021-06-01T00:00:00Z"}, nil
	} else {
		return nil, &pgconn.PgError{Code: "23505"}
	}
}

func (r *testRepository) UpdatePassword(updatedUser dtos.UpdateUser) (*dtos.User, error) {
	if updatedUser.Username == "test" && updatedUser.CurrentPassword == "test" {
		return &user, nil
	} else {
		return nil, errors.New("")
	}
}

func (r *testRepository) DeleteUser(id uint) (*dtos.User, error) {
	if id == 1 {
		return &user, nil
	} else {
		return nil, errors.New("user not found")
	}
}

func (r *testRepository) VerifyUser(username string, password string) bool {
	if username == "test" && password == "test" {
		return true
	} else {
		return false
	}
}

func executeRequest(req *http.Request, r *chi.Mux) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	r.ServeHTTP(rr, req)

	return rr
}

func generateTokenTest(username string, t *testing.T) string {
	_, tokenString, err := auth.GetToken().Encode(map[string]interface{}{
		"username": username,
		"exp":      time.Now().Add(time.Hour).Format(time.RFC3339),
		"iat":      time.Now().Format(time.RFC3339),
	})
	if err != nil {
		t.Error(err)
	}
	return tokenString
}

func TestGetUser_id1(t *testing.T) {
	r := InitRouter()

	req, _ := http.NewRequest("GET", "/api/v1/users/1", nil)

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test", t))

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusOK, respone.Code)

	userJsonString, _ := json.Marshal(user)

	assert.Equal(t, string(userJsonString)+"\n", respone.Body.String())
}

func TestGetUser_id2(t *testing.T) {
	r := InitRouter()

	req, _ := http.NewRequest("GET", "/api/v1/users/2", nil)

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test", t))

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, 404, respone.Code)
}

func TestGetUsers(t *testing.T) {
	r := InitRouter()

	req, _ := http.NewRequest("GET", "/api/v1/users", nil)

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test", t))

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusOK, respone.Code)

	usersJsonString, _ := json.Marshal([]dtos.User{user})

	assert.Equal(t, string(usersJsonString)+"\n", respone.Body.String())
}

func TestCreateUser(t *testing.T) {
	r := InitRouter()

	createUser := dtos.CreateUser{Username: "test2", Password: "test"}

	createUserJsonString, _ := json.Marshal(createUser)

	req, _ := http.NewRequest("POST", "/api/v1/users", bytes.NewReader(createUserJsonString))

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test", t))
	req.Header.Add("Content-Type", "application/json")

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusOK, respone.Code)

	assert.Equal(t, "{\"id\":2,\"username\":\"test2\",\"created_at\":\"2021-06-01T00:00:00Z\",\"updated_at\":\"2021-06-01T00:00:00Z\"}\n", respone.Body.String())
}

func TestCreateUser_duplicate(t *testing.T) {
	r := InitRouter()

	createUser := dtos.CreateUser{Username: "test", Password: "test"}

	createUserJsonString, _ := json.Marshal(createUser)

	req, _ := http.NewRequest("POST", "/api/v1/users", bytes.NewReader(createUserJsonString))

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test", t))
	req.Header.Add("Content-Type", "application/json")

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusConflict, respone.Code)
}

func TestCreateUser_invalidJson(t *testing.T) {
	r := InitRouter()

	createUserJsonString := []byte("invalid")

	req, _ := http.NewRequest("POST", "/api/v1/users", bytes.NewReader(createUserJsonString))

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test", t))
	req.Header.Add("Content-Type", "application/json")

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusBadRequest, respone.Code)
}

func TestUpdatePassword(t *testing.T) {
	r := InitRouter()

	updateUser := dtos.UpdateUser{Username: "test", CurrentPassword: "test", Password: "test"}

	updateUserJsonString, _ := json.Marshal(updateUser)

	req, _ := http.NewRequest("PUT", "/api/v1/users", bytes.NewReader(updateUserJsonString))

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test", t))
	req.Header.Add("Content-Type", "application/json")

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusOK, respone.Code)
}

func TestUpdatePassword_invalidJson(t *testing.T) {
	r := InitRouter()

	updateUserJsonString := []byte("invalid")

	req, _ := http.NewRequest("PUT", "/api/v1/users", bytes.NewReader(updateUserJsonString))

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test", t))
	req.Header.Add("Content-Type", "application/json")

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusBadRequest, respone.Code)
}

func TestUpdatePassword_wrongPassword(t *testing.T) {
	r := InitRouter()

	updateUser := dtos.UpdateUser{Username: "test", CurrentPassword: "wrong", Password: "test"}

	updateUserJsonString, _ := json.Marshal(updateUser)

	req, _ := http.NewRequest("PUT", "/api/v1/users", bytes.NewReader(updateUserJsonString))

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test", t))
	req.Header.Add("Content-Type", "application/json")

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusBadRequest, respone.Code)
}

// Test to change Password of a user that does not exist
func TestUpdatePassword_userNotFound(t *testing.T) {
	r := InitRouter()

	updateUser := dtos.UpdateUser{Username: "test2", CurrentPassword: "test", Password: "test"}

	updateUserJsonString, _ := json.Marshal(updateUser)

	req, _ := http.NewRequest("PUT", "/api/v1/users", bytes.NewReader(updateUserJsonString))

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test2", t))
	req.Header.Add("Content-Type", "application/json")

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusBadRequest, respone.Code)
}

func TestDeleteUser(t *testing.T) {
	r := InitRouter()

	req, _ := http.NewRequest("DELETE", "/api/v1/users/1", nil)

	req.Header.Add("Authorization", "Bearer "+generateTokenTest("test", t))

	handler.Repo = &testRepository{}

	respone := executeRequest(req, r)

	assert.Equal(t, http.StatusOK, respone.Code)
}
