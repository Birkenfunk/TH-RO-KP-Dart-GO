package routes

import (
	"github.com/go-chi/chi/v5"
)

func RegisterHandler() chi.Router {
	r := chi.NewRouter()
	r.Mount("/v1", RegisterHandlerV1())
	return r
}
