package handler

import "net/http"

// HealthCheck
//
//	@Summary		HealthCheck
//	@Description	HealthCheck
//	@Tags			HealthCheck
//	@Produce		json
//	@Success		200	{object}	string
//	@Router			/health [get]
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("OK"))
}
