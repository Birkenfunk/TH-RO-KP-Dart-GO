package handler

import (
	"encoding/json"
	"github.com/go-chi/jwtauth/v5"
	"net/http"
	"th-rosenheim/kp/backend/dtos"
	"th-rosenheim/kp/backend/handler/auth"
	"time"
)

// Login
//	@Summary		Login
//	@Description	Login
//	@Tags			Login
//	@Accept			json
//	@Produce		json
//	@Param			login	body		dtos.Login	true	"Login"
//	@Success		200		{object}	string
//	@Failure		400		{object}	string
//	@Router			/login [post]
func Login(w http.ResponseWriter, r *http.Request) {
	var login dtos.Login
	err := json.NewDecoder(r.Body).Decode(&login)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if !Repo.VerifyUser(login.Username, login.Password) {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	token, err := generateToken(login.Username)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"token": token})
}

// Renew
//	@Summary		Renew
//	@Description	Renew token
//	@Tags			Login
//	@Produce		json
//	@Success		200	{object}	string
//	@Failure		400	{object}	string
//	@Security		Authorization
//	@Router			/login/renew [get]
func Renew(w http.ResponseWriter, r *http.Request) {
	_, claims, _ := jwtauth.FromContext(r.Context())
	newToken, err := generateToken(claims["username"].(string))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"token": newToken})
}

func generateToken(username string) (string, error) {
	_, tokenString, err := auth.GetToken().Encode(map[string]interface{}{
		"username": username,
		"exp":      time.Now().Add(time.Hour).Format(time.RFC3339),
		"iat":      time.Now().Format(time.RFC3339),
	})
	if err != nil {
		return "", err
	}
	return tokenString, nil
}
