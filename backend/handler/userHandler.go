package handler

import (
	"encoding/json"
	"errors"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"net/http"
	"strconv"
	"th-rosenheim/kp/backend/db"
	"th-rosenheim/kp/backend/dtos"
)

var Repo db.Repository

// Get user by id
//	@Summary		Get user by id
//	@Description	Get user by id
//	@Tags			User
//	@Produce		json
//	@Param			id	path		int	true	"User ID"
//	@Success		200	{object}	dtos.User
//	@Failure		400	{object}	string
//	@Failure		404	{object}	string
//	@Security		Authorization
//	@Router			/users/{id} [get]
func GetUserById(w http.ResponseWriter, r *http.Request) {
	//get id from url
	id := chi.URLParam(r, "id")

	//convert id to int
	idInt, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	//get user from db
	user := Repo.GetUserById(uint(idInt))

	//check if user exists
	if user == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	//write user to response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}

// Get users
//	@Summary		Get users
//	@Description	Get all users
//	@Tags			User
//	@Produce		json
//	@Success		200	{array}	dtos.User
//	@Security		Authorization
//	@Router			/users [get]
func GetUsers(w http.ResponseWriter, r *http.Request) {
	//get users from db
	users := Repo.GetUsers()

	//write users to response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(users)
}

// Create user
//	@Summary		Create user
//	@Description	Create user
//	@Tags			User
//	@Accept			json
//	@Produce		json
//	@Param			user	body		dtos.CreateUser	true	"User"
//	@Success		200		{object}	dtos.User
//	@Failure		400		{object}	string
//	@Failure		409		{object}	string
//	@Router			/users [post]
func CreateUser(w http.ResponseWriter, r *http.Request) {
	//get user from request
	var createUser dtos.CreateUser
	err := json.NewDecoder(r.Body).Decode(&createUser)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	//create user in db
	user, err := Repo.CreateUser(createUser)
	//check if error is duplicate error
	if err != nil {
		var pgErr *pgconn.PgError
		if errors.As(err, &pgErr) && pgErr.Code == "23505" {
			w.WriteHeader(http.StatusConflict)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	//write user to response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}

// Update password
//	@Summary		Update password
//	@Description	Update password
//	@Tags			User
//	@Accept			json
//	@Produce		json
//	@Param			user	body		dtos.UpdateUser	true	"User"
//	@Success		200		{object}	dtos.User
//	@Failure		400		{object}	string
//	@Security		Authorization
//	@Router			/users [put]
func UpdatePassword(w http.ResponseWriter, r *http.Request) {
	//get user from request
	var updateUser dtos.UpdateUser
	err := json.NewDecoder(r.Body).Decode(&updateUser)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	//get username from token
	_, claims, _ := jwtauth.FromContext(r.Context())
	username := claims["username"].(string)
	if username != updateUser.Username {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	//update user in db
	user, err := Repo.UpdatePassword(updateUser)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	//write user to response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}

// Delete user
//	@Summary		Delete user
//	@Description	Delete user
//	@Tags			User
//	@Produce		json
//	@Param			id	path		int	true	"User ID"
//	@Success		200	{object}	dtos.User
//	@Failure		400	{object}	string
//	@Security		Authorization
//	@Router			/users/{id} [delete]
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	//get id from url
	id := chi.URLParam(r, "id")

	//convert id to int
	idInt, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	//delete user from db
	user, err := Repo.DeleteUser(uint(idInt))

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	//write user to response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}

func SetRepo() {
	if Repo == nil {
		Repo = db.CreateRepo()
	}
}
