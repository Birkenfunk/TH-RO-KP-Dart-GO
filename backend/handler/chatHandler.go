package handler

import (
	"context"
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"net/http"
	"sync"
	"th-rosenheim/kp/backend/dtos"
)

type connection struct {
	writer     http.ResponseWriter
	flusher    http.Flusher
	requestCtx context.Context
	room       string
	user       string
}

// Server - instance used to accept connections and store them
type Server struct {
	connections      map[string]*connection
	connectionsMutex sync.RWMutex
}

var s = Server{
	connections: make(map[string]*connection),
}

// Register
//	@Summary		Register
//	@Description	Register
//	@Tags			Chat
//	@Produce		text/event-stream
//	@Param			room	path		string	true	"Room"
//	@Success		200		{object}	string
//	@Failure		400		{object}	string
//	@Security		Authorization
//	@Router			/chat/register/{room} [get]
func Register(w http.ResponseWriter, r *http.Request) {
	room := chi.URLParam(r, "room")

	_, claims, _ := jwtauth.FromContext(r.Context())
	user := claims["username"].(string)

	if s.isUserInRoom(user, room) {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("You are already in this room!"))
		return
	}
	w.Header().Set("Content-Type", "text/event-stream")
	flusher, ok := w.(http.Flusher)

	if !ok {
		http.Error(w, "Streaming unsupported!", http.StatusInternalServerError)
		return
	}

	requestContext := r.Context()
	s.addConnection(r.RemoteAddr, w, flusher, requestContext, room, user)

	defer func() {
		s.removeConnection(r.RemoteAddr)
		s.sendMessageToRoom(room, user+" left the chat")
	}()

	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	s.sendMessageToRoom(room, user+" joined the chat")

	<-requestContext.Done()
}

// SendMessage
//	@Summary		Send message
//	@Description	Send message
//	@Tags			Chat
//	@Accept			json
//	@Param			room	path		string				true	"Room"
//	@Param			message	body		dtos.ChatMessage	true	"Message"
//	@Success		200		{object}	string
//	@Failure		400		{object}	string
//	@Security		Authorization
//	@Router			/chat/send/{room} [post]
func SendMessage(w http.ResponseWriter, r *http.Request) {
	room := chi.URLParam(r, "room")
	_, claims, _ := jwtauth.FromContext(r.Context())
	user := claims["username"].(string)

	if !s.isUserInRoom(user, room) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("You are not in this room!"))
		return
	}

	var message dtos.ChatMessage

	err := json.NewDecoder(r.Body).Decode(&message)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	s.sendChatMessageToRoom(user, message.Message, room)

}

func (s *Server) sendChatMessageToRoom(user string, message string, room string) {
	s.sendMessageToRoom(room, user+": "+message)
}

func (s *Server) sendMessageToRoom(room string, message string) {
	s.connectionsMutex.RLock()
	for _, connection := range s.connections {
		if connection.room == room {
			connection.writer.Write([]byte("data: " + message + "\n\n"))
			connection.flusher.Flush()
		}
	}
	s.connectionsMutex.RUnlock()
}

// check if User is in Room
func (s *Server) isUserInRoom(user string, room string) bool {
	s.connectionsMutex.RLock()
	for _, connection := range s.connections {
		if connection.room == room && connection.user == user {
			s.connectionsMutex.RUnlock()
			return true
		}
	}
	s.connectionsMutex.RUnlock()
	return false
}

func (s *Server) addConnection(remoteAddr string, w http.ResponseWriter, flusher http.Flusher, requestContext context.Context, room string, user string) {
	s.connectionsMutex.Lock()
	s.connections[remoteAddr] = &connection{
		writer:     w,
		flusher:    flusher,
		requestCtx: requestContext,
		room:       room,
		user:       user,
	}
	s.connectionsMutex.Unlock()
}

func (s *Server) removeConnection(client string) {
	s.connectionsMutex.Lock()
	defer s.connectionsMutex.Unlock()

	delete(s.connections, client)
}
