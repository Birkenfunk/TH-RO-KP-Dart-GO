package auth

import (
	"github.com/go-chi/jwtauth/v5"
	"github.com/stretchr/testify/assert"
	"os"
)

var token *jwtauth.JWTAuth

func GetToken() *jwtauth.JWTAuth {
	if token == nil {
		secret := os.Getenv("JWT_SECRET")
		assert.NotNil(nil, secret)
		token = jwtauth.New("HS512", []byte(secret), nil)
	}
	return token
}
