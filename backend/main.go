package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"net/http"
	"os"
	"strings"
	_ "th-rosenheim/kp/backend/docs"
	"th-rosenheim/kp/backend/handler"
	"th-rosenheim/kp/backend/routes"
)

func init() {
	fmt.Println("Starting server...")
	godotenv.Load(".env", "test.env")

	dbType := os.Getenv("DB_TYPE")
	if !(dbType == "" || strings.EqualFold(dbType, "Sqlite")) {
		dbUser := os.Getenv("DB_USER")
		dbPassword := os.Getenv("DB_PASSWORD")
		dbHost := os.Getenv("DB_HOST")
		dbPort := os.Getenv("DB_PORT")
		dbName := os.Getenv("DB_NAME")
		if dbUser == "" || dbPassword == "" || dbHost == "" || dbPort == "" || dbName == "" {
			panic("Missing database environment variables")
		}
	}
	if os.Getenv("ENV") == "test" {
		fmt.Println("Running in test mode")
	}
	if os.Getenv("JWT_SECRET") == "" {
		panic("Missing JWT_SECRET environment variable")
	}

	handler.SetRepo()
}

//	@title						KP Backend API
//	@description				This is the API for the KP project of the TH Rosenheim.
//	@BasePath					/api/v1
//	@securityDefinitions.apikey	Authorization
//	@in							header
//	@name						Authorization
func main() {

	r := routes.InitRouter()

	http.ListenAndServe(":3000", r)

}

func HelloWorld() string {
	return "Hello World!"
}
