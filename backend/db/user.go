package db

import (
	"errors"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
	"th-rosenheim/kp/backend/dtos"
	"th-rosenheim/kp/backend/models"
)

type Repository interface {
	GetUserById(id uint) *dtos.User
	GetUsers() *[]dtos.User
	CreateUser(createUser dtos.CreateUser) (*dtos.User, error)
	UpdatePassword(updatedUser dtos.UpdateUser) (*dtos.User, error)
	DeleteUser(id uint) (*dtos.User, error)
	VerifyUser(username string, password string) bool
}

type repository struct {
	DB *gorm.DB
}

func (r *repository) GetUserById(id uint) *dtos.User {
	var user models.User
	err := r.DB.First(&user, id).Error

	if err != nil {
		return nil
	}
	return user.ToDtoUser()
}

func (r *repository) GetUsers() *[]dtos.User {
	var users []models.User
	r.DB.Find(&users)
	var dtoUsers []dtos.User
	for _, user := range users {
		dtoUsers = append(dtoUsers, *user.ToDtoUser())
	}
	return &dtoUsers
}

func (r *repository) CreateUser(createUser dtos.CreateUser) (*dtos.User, error) {
	hashedPassword, err := hashPassword(createUser.Password)
	if err != nil {
		return nil, err
	}
	user := models.User{
		Username: createUser.Username,
		Password: hashedPassword,
	}
	err = r.DB.Create(&user).Error
	if err != nil {
		return nil, err
	}
	return user.ToDtoUser(), nil
}

func (r *repository) UpdatePassword(updatedUser dtos.UpdateUser) (*dtos.User, error) {
	hashedPassword, err := hashPassword(updatedUser.Password)
	if err != nil {
		return nil, err
	}
	var user models.User
	err = r.DB.Where("username = ?", updatedUser.Username).First(&user).Error
	if err != nil {
		return nil, err
	}
	if !checkPasswordHash(updatedUser.CurrentPassword, user.Password) {
		return nil, errors.New("wrong password")
	}
	user.Password = hashedPassword
	r.DB.Save(&user)
	return user.ToDtoUser(), nil
}

func (r *repository) DeleteUser(id uint) (*dtos.User, error) {
	var user models.User
	err := r.DB.First(&user, id).Error
	if err != nil {
		return nil, err
	}
	err = r.DB.Delete(&user).Error
	if err != nil {
		return nil, err
	}
	return user.ToDtoUser(), nil
}

func (r *repository) VerifyUser(username string, password string) bool {
	var dbUser models.User
	err := r.DB.Where("username = ?", username).First(&dbUser).Error
	if err != nil {
		return false
	}
	return checkPasswordHash(password, dbUser.Password)
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

func checkPasswordHash(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func CreateRepo() Repository {
	return &repository{
		DB: GetDB(),
	}
}
