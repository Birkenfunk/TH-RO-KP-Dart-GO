package db

import (
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"testing"
	"th-rosenheim/kp/backend/dtos"
	"th-rosenheim/kp/backend/models"
)

func init() {
	hashedPassword, err := hashPassword("Password1")
	if err != nil {
		panic(err)
	}
	user1 = models.User{
		Username: "User1",
		Password: hashedPassword,
	}
	hashedPassword, err = hashPassword("Password2")
	if err != nil {
		panic(err)
	}
	user2 = models.User{
		Username: "User2",
		Password: hashedPassword,
	}
}

var user1 models.User

var user2 models.User

func NewMockDB() *gorm.DB {
	initDB, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	initDB.Migrator().DropTable(&models.User{})
	initDB.AutoMigrate(&models.User{})

	initDB.Create(&user1)
	initDB.Create(&user2)

	return initDB
}

func TestRepository_GetUserById(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	user := repo.GetUserById(1)

	assert.Equal(t, uint(1), user.Id)
	assert.Equal(t, "User1", user.Username)
	assert.NotNil(t, user.CreatedAt)
	assert.NotNil(t, user.UpdatedAt)

}

// Test to Get a User that does not exist
func TestRepository_GetUserById_NotFound(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	user := repo.GetUserById(3)

	assert.Nil(t, user)
}

func TestRepository_GetUsers(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	users := *repo.GetUsers()

	assert.Equal(t, 2, len(users))

	assert.Equal(t, uint(1), users[0].Id)
	assert.Equal(t, "User1", users[0].Username)
	assert.NotNil(t, users[0].CreatedAt)
	assert.NotNil(t, users[0].UpdatedAt)

	assert.Equal(t, uint(1), users[0].Id)
	assert.Equal(t, "User1", users[0].Username)
	assert.NotNil(t, users[0].CreatedAt)
	assert.NotNil(t, users[0].UpdatedAt)
}

func TestRepository_CreateUser(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	user, err := repo.CreateUser(dtos.CreateUser{
		Username: "User3",
		Password: "Password3",
	})

	assert.Nil(t, err)
	assert.Equal(t, uint(3), user.Id)
	assert.Equal(t, "User3", user.Username)
	assert.NotNil(t, user.CreatedAt)
	assert.NotNil(t, user.UpdatedAt)
}

// Test to Create a User with an already existing Username
func TestRepository_CreateUser_UsernameAlreadyExists(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	user, err := repo.CreateUser(dtos.CreateUser{
		Username: "User1",
		Password: "Password3",
	})

	assert.NotNil(t, err)
	assert.Nil(t, user)
}

func TestRepository_UpdatePassword(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	user, err := repo.UpdatePassword(dtos.UpdateUser{
		Username:        "User1",
		CurrentPassword: "Password1",
		Password:        "Password3",
	})

	assert.Nil(t, err)
	assert.Equal(t, uint(1), user.Id)
	assert.Equal(t, "User1", user.Username)
	assert.NotNil(t, user.CreatedAt)
	assert.NotNil(t, user.UpdatedAt)

	assert.False(t, repo.VerifyUser("User1", "Password1"))
	assert.True(t, repo.VerifyUser("User1", "Password3"))
}

// Test to Update a User that does not exist
func TestRepository_UpdatePassword_NotFound(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	user, err := repo.UpdatePassword(dtos.UpdateUser{
		Username:        "User5",
		CurrentPassword: "Password1",
		Password:        "Password3",
	})

	assert.NotNil(t, err)
	assert.Nil(t, user)
}

func TestRepository_DeleteUser(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	user, err := repo.DeleteUser(1)

	assert.Nil(t, err)
	assert.Equal(t, uint(1), user.Id)
	assert.Equal(t, "User1", user.Username)
	assert.NotNil(t, user.CreatedAt)
	assert.NotNil(t, user.UpdatedAt)

	user = repo.GetUserById(1)

	assert.Nil(t, user)
}

// Test to Change the Password with a wrong CurrentPassword
func TestRepository_UpdatePassword_WrongPassword(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	user, err := repo.UpdatePassword(dtos.UpdateUser{
		Username:        "User1",
		CurrentPassword: "Password2",
		Password:        "Password3",
	})

	assert.NotNil(t, err)
	assert.Nil(t, user)
}

// Test to Delete a User that does not exist
func TestRepository_DeleteUser_NotFound(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	user, err := repo.DeleteUser(3)

	assert.NotNil(t, err)
	assert.Nil(t, user)
}

func TestRepository_VerifyUser(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	assert.True(t, repo.VerifyUser("User1", "Password1"))
}

func TestRepository_VerifyUser_False(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	assert.False(t, repo.VerifyUser("User1", "Password2"))
}

// Test to Verify a User that does not exist
func TestRepository_VerifyUser_NotFound(t *testing.T) {
	db := NewMockDB()

	repo := repository{DB: db}

	assert.False(t, repo.VerifyUser("User3", "Password3"))
}

func Test_checkPasswordHash(t *testing.T) {
	hashedPassword, err := hashPassword("Password1")

	assert.Nil(t, err)
	assert.NotNil(t, hashedPassword)
	assert.NotEqual(t, "Password1", hashedPassword)

	assert.True(t, checkPasswordHash("Password1", hashedPassword))
	assert.False(t, checkPasswordHash("Password2", hashedPassword))
}

// Test create Repository
func TestCreateRepo(t *testing.T) {
	db = NewMockDB()

	repo := CreateRepo()

	assert.Equal(t, db, repo.(*repository).DB)
}
