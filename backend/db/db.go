package db

import (
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"os"
	"strings"
	"th-rosenheim/kp/backend/models"
)

var db *gorm.DB

func initDB() {
	dbType := os.Getenv("DB_TYPE")
	if dbType == "" || strings.EqualFold(dbType, "Sqlite") {
		initSqlite()
	} else if strings.EqualFold(dbType, "Mysql") {
		initMysql()
	} else if strings.EqualFold(dbType, "Postgres") {
		initPostgres()
	} else {
		panic("Unknown DB_TYPE")
	}
}

func initMysql() {
	dsn := os.Getenv("DB_USER") + ":" + os.Getenv("DB_PASSWORD") + "@tcp(" +
		os.Getenv("DB_HOST") + ":" + os.Getenv("DB_PORT") + ")/" + os.Getenv("DB_NAME") +
		"?charset=utf8mb4&parseTime=True&loc=Local"
	initDb, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	err = initDb.AutoMigrate(&models.User{})
	if err != nil {
		panic(err)
	}
	db = initDb
}

func initPostgres() {
	dsn := "host=" + os.Getenv("DB_HOST") + " user=" + os.Getenv("DB_USER") +
		" password=" + os.Getenv("DB_PASSWORD") + " dbname=" + os.Getenv("DB_NAME") +
		" port=" + os.Getenv("DB_PORT") + " sslmode=disable"
	initDb, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	err = initDb.AutoMigrate(&models.User{})
	if err != nil {
		panic(err)
	}
	db = initDb
}

func initSqlite() {
	initDB, err := gorm.Open(sqlite.Open("user.db"), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	initDB.AutoMigrate(&models.User{})
	db = initDB
}

func GetDB() *gorm.DB {
	if db == nil {
		initDB()
	}
	return db
}
